//Primer arreglo
var array20 = [50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69];
function func1(array20){
    var tot = 0,i;
    for(i=0; i<array20.length; i++){
        console.log(array20[i]);
        tot = tot + array20[i];
    }
    var prod = tot/20;
    return prod;
};
console.log(func1(array20));

function imprimir20(){
    let parrafo1 = document.getElementById('parrafo1');
    parrafo1.innerHTML = parrafo1.innerHTML + "Los datos son: " + array20 + "<br/>" + "El promedio es: " + func1(array20) + "<br/><br/>" ;
    
}

//Arreglo de pares
var arrayPar = [2,3,4,5,13,34,32,56,78,1,76,12,11,50,40,18,19,1,17,34];
function func2(arrayPar){
    var tot = 0,i;
    for(i=0; i<arrayPar.length; i++){
        if((arrayPar[i]%2)==0){
            tot = tot+1;
        }
    }
    return tot;
}

function imprimirPares(){
    let parrafo2 = document.getElementById('parrafo2');
    parrafo2.innerHTML = parrafo2.innerHTML + "Los datos son: " + arrayPar + "<br/>" + "El promedio es: " + func1(arrayPar) + "<br/><br/>" ;
    
}
//Arreglo de lista ordenada
var arrayLista = [10,20,30,80,70,50,90,40,60,100,140,120,130,300,200,600,700,900,150,160];
function func3(arrayLista) {
    
    var n, i, k, au;
    n = arrayLista.length;
    console.log("Lista desordenada:"+arrayLista); 

    for (k = 1; k < n; k++) {
        for (i = 0; i < (n - k); i++) {
            if (arrayLista[i] > arrayLista[i + 1]) {
                au = arrayLista[i];
                arrayLista[i] = arrayLista[i + 1];
                arrayLista[i + 1] = au;
            }
        }
    }

    return arrayLista;
}
function imprimirLista(){
    let parrafo3 = document.getElementById('parrafo3');
    parrafo3.innerHTML = parrafo3.innerHTML + "Los datos son: " + arrayLista + "<br/>" + "El promedio es: " + func1(arrayLista) + "<br/><br/>" ;
    
}

// Generacion de Objetos
function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}
function generar(){
    let limite = document.getElementById('limite').value;
    let lstnumeros = document.getElementById('idNumeros');
    let parp = document.getElementById('par')
    let impar = document.getElementById('impar');
    var numero = getRandomArbitrary(1,51);
    var arreglo = new Array(100);
    var pares = 0;
    var impares = 0;
    for (let con = 0; con < limite; ++ con) {
        let aleatorio = Math.floor(Math.random()*50)+1;
        lstnumeros.options[con] = new Option(aleatorio, 'valor:'+ con);

    }
    // lenado del arreglo de numeros impares
    for (let i = 0; i < arreglo.length; i++) {
        arreglo[i] = getRandomInt(101);  
        console.log(arreglo[i]);
    }
    //contando cantidad de numero pares e impares
    for (let i = 0; i < arreglo.length; i++) {
        if ((arreglo[i] % 2) == 0) {
            pares = pares + 1;
        }else{
            impares = impares + 1;
        }
    }
    parp.innerHTML = ("");
    parp.innerHTML = parp.innerHTML + "" + pares +"%";
    impar.innerHTML = ("");
    impar.innerHTML = impar.innerHTML + "" + impares + "%";

    //Saber si es Simetrico
    porPar2=porPar.toFixed(0);
    parIm2=porIm.toFixed(0);

    let simetrico = document.getElementById('simetrico');
    if(parp==75 || impar==75){
        simetrico.innerHTML = simetrico.innerHTML + "Sí es simétrico";
        
    }else{
        simetrico.innerHTML = simetrico.innerHTML + "No es simétrico";
    }
}
